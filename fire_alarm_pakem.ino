// 27 april
// berhasil publish dan ditelpon,tapii sayaratnya ftdi nyambun ke spiker, ftdi copot publis masih bisa
#include <SoftwareSerial.h>
#include <SIM900Client.h>
#include <PubSubClient.h>
#include <GSM_Shield.h>

GSM gsm;
unsigned long timedelay = millis();
const int interval = 5000;

SIM900Client client(2, 3, 6);
byte server[]     = { 128, 199, 252, 139 };

PubSubClient mqtt(server, 1883, callback, client);

//digital input
const int buttonPin = 1;     // the number of the pushbutton pin
int buttonState = 0;         // variable for reading the pushbutton status
int kondisi = HIGH;
char p[1];

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  koneksi(); 
}

void loop()
{
  if ((mqtt.connected()) && (client.connected())){
    if (tombol_kebakaran()){
      if(mqtt.publish("alarm/0001/status",p)){
        //client.flush();
        Serial.println(F("published"));  
        terima_telpon();          
      }else{
        Serial.println(F("ora published"));
      }
    }
  }else{
    Serial.println(F("diskonek bro"));
    mqtt.disconnect();
    client.stop();  
    koneksi(); 
  }
    mqtt.loop(); //urg masukin ini gamasalah 
}
 // mqtt.loop();

void koneksi(){
  gsm.TurnOn(9600);              //module power on
  gsm.InitParam(PARAM_SET_1);//configure the module  
  gsm.Echo(1);               //enable AT echo  
  gsm.TurnOn(9600);  
  gsm.Echo(0);               //enable AT echo    , settingan labil, tp akhirnya bisa ditelp 
  gsm.InitParam(PARAM_SET_1);
  client.begin(9600);
  if (!client) {
    Serial.println(F("SIM900 could not be initialized. Check power and baud rate."));
    for (;;);
  }
  while (!client.attach("3data", "3data", "3data")) {
    Serial.println(F("Could not attach GPRS."));
  }

  while (!mqtt.connect("OK")) {
    Serial.println(F("mqtt gagal"));
  }
  
  Serial.println(F("connected"));
}

void callback(char* topic, byte* payload, unsigned int length) {
}

//FUNGSI TOMBOL
boolean tombol_kebakaran()
{
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  
  if (buttonState != kondisi){
    if (buttonState == HIGH){
     Serial.println(F("aman berooo..."));
     kondisi =HIGH;  
     sprintf(p, "%d", LOW);    
    }
    else if (buttonState == LOW){
     Serial.println(F("KEBAKARAN"));
     kondisi = LOW;
     sprintf(p, "%d", HIGH);
    }
    return true;
  }else{
    return false;
  }
  
}

void terima_telpon(){
    
    int call;
    call=gsm.CallStatus();
    switch (call){    
      case CALL_NONE:
        Serial.println("no call");
        break;
      case CALL_INCOM_VOICE:
        Serial.println("incoming voice call"); 
        delay(5000);     
        gsm.PickUp();
        break;
      case CALL_ACTIVE_VOICE:
        Serial.println("active voice call");
        delay(5000);     
        gsm.HangUp();
        break;
      case CALL_NO_RESPONSE:
        Serial.println("no response");
        break;
    }
    delay(1000);
}
    
